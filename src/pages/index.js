import React from 'react'
import { Component } from 'react'
import schema from '../lib/schema'
import AutoForm from 'uniforms-antd/AutoForm'
import DateRange from '../components/DateRange'
// import 'antd/lib/date-picker/style/css'
import 'antd/dist/antd.css'
import TimeRange from '../components/TimeRange'
import '../components/styles.css'
// import Repeater from "./repeater/Repeater";
// import { ErrorsField, SubmitField } from "uniforms-antd";

export default class App extends Component {
  onChange = (...args) => {
    console.log('model onChange', ...args)
  }

  render() {
    return (
      <div className="container">
        <AutoForm
          label={false}
          onChange={this.onChange}
          schema={schema}
          spacing={3}
          onSubmit={model => console.log('onSubmit', model)}
          onSubmitFailure={model => console.log('onSubmitFailure', model)}
          onSubmitSuccess={model => console.log('onSubmitSuccess', model)}
        >
          <section className="section">
            <DateRange name="dateRange" />
          </section>
          <section className="section">
            <TimeRange name="timeRange" />
          </section>
          <section className="section">
            {/* <Repeater
              name="repeater"
              list={[
                "List  item  number  one",
                "List  item  number  two",
                "List  item  number  three",
                "List  item  number  four"
              ]}
            /> */}
          </section>
          {/* <ErrorsField /> <SubmitField /> */}
        </AutoForm>
      </div>
    )
  }
}
