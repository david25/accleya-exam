import React from 'react'
import { TimePicker, Button } from 'antd'
import moment from 'moment'

export default class TimeRange extends React.Component {
  state = {
    start: '',
    end: '',
    open: false,
  }
  onStartChange = (time, timeString) => {
    this.setState({
      start: timeString,
    })
  }

  onEndChange = (time, timeString) => {
    this.setState({
      end: timeString,
      open: true,
    })
  }

  handleOpenChange = () => {
    this.setState({
      open: true,
    })
  }

  handleConfirm = () => {
    this.setState({
      open: false,
    })
  }

  render() {
    const { start, end, open } = this.state
    return (
      <div>
        <TimePicker
          open={open}
          onOpenChange={this.handleOpenChange}
          className="timePicker"
          onChange={this.onStartChange}
          defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}
        />
        <TimePicker
          open={open}
          className="timePicker"
          onChange={this.onEndChange}
          defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}
        />
        <Button onClick={this.handleConfirm}>Confirm</Button>
      </div>
    )
  }
}
