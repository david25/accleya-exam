import React from 'react'
import { DatePicker } from 'antd'
const { RangePicker } = DatePicker

export default class DateRange extends React.Component {
  onChange = (date, dateString) => {
    const result = { startDate: dateString[0], endDate: dateString[1] }
    console.log('result', result)
  }

  onOk() {
    console.log('Okay?')
  }

  render() {
    return (
      <div>
        <RangePicker
          format="YYYY-MM-DD HH:mm"
          placeholder={['Start Time', 'End Time']}
          onChange={this.onChange}
          onOk={this.onOk}
        />
      </div>
    )
  }
}
